#ifndef _MAINWINDOW_H
#define _MAINWINDOW_H

#include <QMainWindow>

#include "ui_mainwindow.h"

class CMainWindow : public QMainWindow, Ui::MainWindow
{
	Q_OBJECT

	private:
		QPixmap AssebleCubeMap(void);
		void OpenSideImage(QLineEdit *edit, QString title);

	public:
		CMainWindow(QWidget *parent = 0);
		~CMainWindow();

	protected slots:
		void Save(void);

		void OpenPXImage(void)	{ OpenSideImage(PXEdit, tr("Positive X")); }
		void OpenNXImage(void)	{ OpenSideImage(NXEdit, tr("Negative X")); }
		void OpenPYImage(void)	{ OpenSideImage(PYEdit, tr("Positive Y")); }
		void OpenNYImage(void)	{ OpenSideImage(NYEdit, tr("Negative Y")); }
		void OpenPZImage(void)	{ OpenSideImage(PZEdit, tr("Positive Z")); }
		void OpenNZImage(void)	{ OpenSideImage(NZEdit, tr("Negative Z")); }
};

#endif
