#include "mainwindow.h"

#include <QFileDialog>
#include <QMessageBox>

#define max(a, b) (a > b ? a : b)



CMainWindow::CMainWindow(QWidget *parent) : QMainWindow(parent), Ui::MainWindow()
{
	setupUi(this);

	connect(SaveButton, SIGNAL(clicked()), this, SLOT(Save()));

	connect(PXOpenFileButton, SIGNAL(clicked()), this, SLOT(OpenPXImage()));
	connect(NXOpenFileButton, SIGNAL(clicked()), this, SLOT(OpenNXImage()));
	connect(PYOpenFileButton, SIGNAL(clicked()), this, SLOT(OpenPYImage()));
	connect(NYOpenFileButton, SIGNAL(clicked()), this, SLOT(OpenNYImage()));
	connect(PZOpenFileButton, SIGNAL(clicked()), this, SLOT(OpenPZImage()));
	connect(NZOpenFileButton, SIGNAL(clicked()), this, SLOT(OpenNZImage()));
}

CMainWindow::~CMainWindow()
{
}

void CMainWindow::Save(void)
{
	QString filename = QFileDialog::getSaveFileName(this, tr("Save CubeMap"));

	if(filename.length() == 0)
		return;

	QPixmap out = AssebleCubeMap();

	out.save(filename);
}

#include <QPainter>


/*
 * ----------------
 * | -x | -z | +x |
 * ----------------
 * | -y | +y | +z |
 * ----------------
 */

QPixmap CMainWindow::AssebleCubeMap(void)
{
	QPixmap sides[6];
	int side_width, side_height;
	bool different_size = false;

	sides[0].load(PXEdit->text());
	sides[1].load(NXEdit->text());
	sides[2].load(PYEdit->text());
	sides[3].load(NYEdit->text());
	sides[4].load(PZEdit->text());
	sides[5].load(NZEdit->text());

	int width = 0;
	int height = 0;

	for(int i=0; i<6; i++)
	{
		side_width = sides[i].width();
		side_height = sides[i].height();

		if(i > 0 && !different_size)
		{
			if(side_width != width || side_height != height)
			{
				different_size = true;
				QMessageBox::information(this, tr("Save CubeMap"), tr("Side images have different sizes or one side could not be loaded.\nTrying to create CubeMap anyway..."));
			}
		}

		width = max(width, side_width);
		height = max(height, side_height);
	}

	QPixmap out(width * 3, height * 2);
	QPainter *painter = new QPainter(&out);

	painter->fillRect(out.rect(), Qt::black);

	painter->drawPixmap(0, 0, sides[1]);
	painter->drawPixmap(width, 0, sides[5]);
	painter->drawPixmap(width * 2, 0, sides[0]);
	painter->drawPixmap(0, height, sides[3]);
	painter->drawPixmap(width, height, sides[2]);
	painter->drawPixmap(width * 2, height, sides[4]);

	delete painter;

	return out;
}

void CMainWindow::OpenSideImage(QLineEdit *edit, QString title)
{
	QString filename = QFileDialog::getOpenFileName(this, tr("Open ") + title);

	if(filename.length() > 0)
		edit->setText(filename);
}
